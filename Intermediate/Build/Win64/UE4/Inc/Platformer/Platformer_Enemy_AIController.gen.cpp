// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Platformer/Platformer_Enemy_AIController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlatformer_Enemy_AIController() {}
// Cross Module References
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformer_Enemy_AIController_NoRegister();
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformer_Enemy_AIController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_Platformer();
// End Cross Module References
	DEFINE_FUNCTION(APlatformer_Enemy_AIController::execMovingSomwhere)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MovingSomwhere();
		P_NATIVE_END;
	}
	void APlatformer_Enemy_AIController::StaticRegisterNativesAPlatformer_Enemy_AIController()
	{
		UClass* Class = APlatformer_Enemy_AIController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "MovingSomwhere", &APlatformer_Enemy_AIController::execMovingSomwhere },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Platformer_Enemy_AIController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformer_Enemy_AIController, nullptr, "MovingSomwhere", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APlatformer_Enemy_AIController_NoRegister()
	{
		return APlatformer_Enemy_AIController::StaticClass();
	}
	struct Z_Construct_UClass_APlatformer_Enemy_AIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_Platformer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APlatformer_Enemy_AIController_MovingSomwhere, "MovingSomwhere" }, // 2999174809
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Platformer_Enemy_AIController.h" },
		{ "ModuleRelativePath", "Platformer_Enemy_AIController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlatformer_Enemy_AIController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::ClassParams = {
		&APlatformer_Enemy_AIController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlatformer_Enemy_AIController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlatformer_Enemy_AIController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlatformer_Enemy_AIController, 263679494);
	template<> PLATFORMER_API UClass* StaticClass<APlatformer_Enemy_AIController>()
	{
		return APlatformer_Enemy_AIController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlatformer_Enemy_AIController(Z_Construct_UClass_APlatformer_Enemy_AIController, &APlatformer_Enemy_AIController::StaticClass, TEXT("/Script/Platformer"), TEXT("APlatformer_Enemy_AIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlatformer_Enemy_AIController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
