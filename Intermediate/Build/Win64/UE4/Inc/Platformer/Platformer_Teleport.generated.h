// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PLATFORMER_Platformer_Teleport_generated_h
#error "Platformer_Teleport.generated.h already included, missing '#pragma once' in Platformer_Teleport.h"
#endif
#define PLATFORMER_Platformer_Teleport_generated_h

#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_SPARSE_DATA
#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBottomTriggerBeginOverlap); \
	DECLARE_FUNCTION(execTopTriggerBeginOverlap);


#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBottomTriggerBeginOverlap); \
	DECLARE_FUNCTION(execTopTriggerBeginOverlap);


#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlatformer_Teleport(); \
	friend struct Z_Construct_UClass_APlatformer_Teleport_Statics; \
public: \
	DECLARE_CLASS(APlatformer_Teleport, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_Teleport)


#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlatformer_Teleport(); \
	friend struct Z_Construct_UClass_APlatformer_Teleport_Statics; \
public: \
	DECLARE_CLASS(APlatformer_Teleport, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_Teleport)


#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlatformer_Teleport(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformer_Teleport) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_Teleport); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_Teleport); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_Teleport(APlatformer_Teleport&&); \
	NO_API APlatformer_Teleport(const APlatformer_Teleport&); \
public:


#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_Teleport(APlatformer_Teleport&&); \
	NO_API APlatformer_Teleport(const APlatformer_Teleport&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_Teleport); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_Teleport); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlatformer_Teleport)


#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_PRIVATE_PROPERTY_OFFSET
#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_13_PROLOG
#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_SPARSE_DATA \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_RPC_WRAPPERS \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_INCLASS \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_SPARSE_DATA \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class APlatformer_Teleport>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_Enviroment_Platformer_Teleport_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
