// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLATFORMER_Platformer_AreaToSpawnBonus_generated_h
#error "Platformer_AreaToSpawnBonus.generated.h already included, missing '#pragma once' in Platformer_AreaToSpawnBonus.h"
#endif
#define PLATFORMER_Platformer_AreaToSpawnBonus_generated_h

#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_SPARSE_DATA
#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_RPC_WRAPPERS
#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlatformer_AreaToSpawnBonus(); \
	friend struct Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics; \
public: \
	DECLARE_CLASS(APlatformer_AreaToSpawnBonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_AreaToSpawnBonus)


#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPlatformer_AreaToSpawnBonus(); \
	friend struct Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics; \
public: \
	DECLARE_CLASS(APlatformer_AreaToSpawnBonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_AreaToSpawnBonus)


#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlatformer_AreaToSpawnBonus(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformer_AreaToSpawnBonus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_AreaToSpawnBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_AreaToSpawnBonus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_AreaToSpawnBonus(APlatformer_AreaToSpawnBonus&&); \
	NO_API APlatformer_AreaToSpawnBonus(const APlatformer_AreaToSpawnBonus&); \
public:


#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_AreaToSpawnBonus(APlatformer_AreaToSpawnBonus&&); \
	NO_API APlatformer_AreaToSpawnBonus(const APlatformer_AreaToSpawnBonus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_AreaToSpawnBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_AreaToSpawnBonus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlatformer_AreaToSpawnBonus)


#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_PRIVATE_PROPERTY_OFFSET
#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_9_PROLOG
#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_SPARSE_DATA \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_RPC_WRAPPERS \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_INCLASS \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_SPARSE_DATA \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class APlatformer_AreaToSpawnBonus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_Enviroment_Platformer_AreaToSpawnBonus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
