// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Platformer/Platformer_GameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlatformer_GameInstance() {}
// Cross Module References
	PLATFORMER_API UFunction* Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Platformer();
	PLATFORMER_API UClass* Z_Construct_UClass_UPlatformer_GameInstance_NoRegister();
	PLATFORMER_API UClass* Z_Construct_UClass_UPlatformer_GameInstance();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstance();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformer_EnemyCharacter_NoRegister();
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformer_Bonus_NoRegister();
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformer_AreaToSpawnBonus_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Platformer, nullptr, "OnBonusUpdate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UPlatformer_GameInstance::execGetRandomPointInArea)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetRandomPointInArea();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlatformer_GameInstance::execSpawnEnemy)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnEnemy();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlatformer_GameInstance::execGenerateBonus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GenerateBonus();
		P_NATIVE_END;
	}
	void UPlatformer_GameInstance::StaticRegisterNativesUPlatformer_GameInstance()
	{
		UClass* Class = UPlatformer_GameInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GenerateBonus", &UPlatformer_GameInstance::execGenerateBonus },
			{ "GetRandomPointInArea", &UPlatformer_GameInstance::execGetRandomPointInArea },
			{ "SpawnEnemy", &UPlatformer_GameInstance::execSpawnEnemy },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlatformer_GameInstance, nullptr, "GenerateBonus", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics
	{
		struct Platformer_GameInstance_eventGetRandomPointInArea_Parms
		{
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Platformer_GameInstance_eventGetRandomPointInArea_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlatformer_GameInstance, nullptr, "GetRandomPointInArea", nullptr, nullptr, sizeof(Platformer_GameInstance_eventGetRandomPointInArea_Parms), Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlatformer_GameInstance, nullptr, "SpawnEnemy", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPlatformer_GameInstance_NoRegister()
	{
		return UPlatformer_GameInstance::StaticClass();
	}
	struct Z_Construct_UClass_UPlatformer_GameInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedEnemy_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnedEnemy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedBonuses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpawnedBonuses;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnedBonuses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnBonus_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnBonus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnAreaBP_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnAreaBP;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBonusUpdate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBonusUpdate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlatformer_GameInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_Platformer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPlatformer_GameInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPlatformer_GameInstance_GenerateBonus, "GenerateBonus" }, // 837342961
		{ &Z_Construct_UFunction_UPlatformer_GameInstance_GetRandomPointInArea, "GetRandomPointInArea" }, // 4206814229
		{ &Z_Construct_UFunction_UPlatformer_GameInstance_SpawnEnemy, "SpawnEnemy" }, // 2058418612
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlatformer_GameInstance_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Platformer_GameInstance.h" },
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedEnemy_MetaData[] = {
		{ "Category", "SpawnSetting" },
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedEnemy = { "SpawnedEnemy", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlatformer_GameInstance, SpawnedEnemy), Z_Construct_UClass_APlatformer_EnemyCharacter_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedEnemy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedEnemy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedBonuses_MetaData[] = {
		{ "Category", "SpawnSetting" },
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedBonuses = { "SpawnedBonuses", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlatformer_GameInstance, SpawnedBonuses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedBonuses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedBonuses_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedBonuses_Inner = { "SpawnedBonuses", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_APlatformer_Bonus_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnBonus_MetaData[] = {
		{ "Category", "SpawnSetting" },
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnBonus = { "SpawnBonus", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlatformer_GameInstance, SpawnBonus), Z_Construct_UClass_APlatformer_Bonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnBonus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnBonus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnAreaBP_MetaData[] = {
		{ "Category", "SpawnSetting" },
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnAreaBP = { "SpawnAreaBP", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlatformer_GameInstance, SpawnAreaBP), Z_Construct_UClass_APlatformer_AreaToSpawnBonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnAreaBP_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnAreaBP_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_OnBonusUpdate_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "Platformer_GameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_OnBonusUpdate = { "OnBonusUpdate", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlatformer_GameInstance, OnBonusUpdate), Z_Construct_UDelegateFunction_Platformer_OnBonusUpdate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_OnBonusUpdate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_OnBonusUpdate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlatformer_GameInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedEnemy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedBonuses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnedBonuses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnBonus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_SpawnAreaBP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlatformer_GameInstance_Statics::NewProp_OnBonusUpdate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlatformer_GameInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlatformer_GameInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlatformer_GameInstance_Statics::ClassParams = {
		&UPlatformer_GameInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPlatformer_GameInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlatformer_GameInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPlatformer_GameInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlatformer_GameInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlatformer_GameInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlatformer_GameInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlatformer_GameInstance, 2009037560);
	template<> PLATFORMER_API UClass* StaticClass<UPlatformer_GameInstance>()
	{
		return UPlatformer_GameInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlatformer_GameInstance(Z_Construct_UClass_UPlatformer_GameInstance, &UPlatformer_GameInstance::StaticClass, TEXT("/Script/Platformer"), TEXT("UPlatformer_GameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlatformer_GameInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
