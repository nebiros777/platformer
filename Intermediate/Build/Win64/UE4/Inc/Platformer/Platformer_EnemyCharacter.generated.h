// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PLATFORMER_Platformer_EnemyCharacter_generated_h
#error "Platformer_EnemyCharacter.generated.h already included, missing '#pragma once' in Platformer_EnemyCharacter.h"
#endif
#define PLATFORMER_Platformer_EnemyCharacter_generated_h

#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_SPARSE_DATA
#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBeginOverlap);


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBeginOverlap);


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlatformer_EnemyCharacter(); \
	friend struct Z_Construct_UClass_APlatformer_EnemyCharacter_Statics; \
public: \
	DECLARE_CLASS(APlatformer_EnemyCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_EnemyCharacter)


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPlatformer_EnemyCharacter(); \
	friend struct Z_Construct_UClass_APlatformer_EnemyCharacter_Statics; \
public: \
	DECLARE_CLASS(APlatformer_EnemyCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_EnemyCharacter)


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlatformer_EnemyCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformer_EnemyCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_EnemyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_EnemyCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_EnemyCharacter(APlatformer_EnemyCharacter&&); \
	NO_API APlatformer_EnemyCharacter(const APlatformer_EnemyCharacter&); \
public:


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_EnemyCharacter(APlatformer_EnemyCharacter&&); \
	NO_API APlatformer_EnemyCharacter(const APlatformer_EnemyCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_EnemyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_EnemyCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlatformer_EnemyCharacter)


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Impulse() { return STRUCT_OFFSET(APlatformer_EnemyCharacter, Impulse); }


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_11_PROLOG
#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_SPARSE_DATA \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_RPC_WRAPPERS \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_INCLASS \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_SPARSE_DATA \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Platformer_EnemyCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class APlatformer_EnemyCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_Platformer_EnemyCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
