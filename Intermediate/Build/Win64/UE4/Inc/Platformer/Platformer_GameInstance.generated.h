// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef PLATFORMER_Platformer_GameInstance_generated_h
#error "Platformer_GameInstance.generated.h already included, missing '#pragma once' in Platformer_GameInstance.h"
#endif
#define PLATFORMER_Platformer_GameInstance_generated_h

#define Platformer_Source_Platformer_Platformer_GameInstance_h_16_DELEGATE \
static inline void FOnBonusUpdate_DelegateWrapper(const FMulticastScriptDelegate& OnBonusUpdate) \
{ \
	OnBonusUpdate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_SPARSE_DATA
#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetRandomPointInArea); \
	DECLARE_FUNCTION(execSpawnEnemy); \
	DECLARE_FUNCTION(execGenerateBonus);


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetRandomPointInArea); \
	DECLARE_FUNCTION(execSpawnEnemy); \
	DECLARE_FUNCTION(execGenerateBonus);


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlatformer_GameInstance(); \
	friend struct Z_Construct_UClass_UPlatformer_GameInstance_Statics; \
public: \
	DECLARE_CLASS(UPlatformer_GameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(UPlatformer_GameInstance)


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUPlatformer_GameInstance(); \
	friend struct Z_Construct_UClass_UPlatformer_GameInstance_Statics; \
public: \
	DECLARE_CLASS(UPlatformer_GameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(UPlatformer_GameInstance)


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlatformer_GameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlatformer_GameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlatformer_GameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlatformer_GameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlatformer_GameInstance(UPlatformer_GameInstance&&); \
	NO_API UPlatformer_GameInstance(const UPlatformer_GameInstance&); \
public:


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlatformer_GameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlatformer_GameInstance(UPlatformer_GameInstance&&); \
	NO_API UPlatformer_GameInstance(const UPlatformer_GameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlatformer_GameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlatformer_GameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlatformer_GameInstance)


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_PRIVATE_PROPERTY_OFFSET
#define Platformer_Source_Platformer_Platformer_GameInstance_h_18_PROLOG
#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_SPARSE_DATA \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_RPC_WRAPPERS \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_INCLASS \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_Platformer_GameInstance_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_SPARSE_DATA \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Platformer_GameInstance_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class UPlatformer_GameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_Platformer_GameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
