// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLATFORMER_PlatformerGameMode_generated_h
#error "PlatformerGameMode.generated.h already included, missing '#pragma once' in PlatformerGameMode.h"
#endif
#define PLATFORMER_PlatformerGameMode_generated_h

#define Platformer_Source_Platformer_PlatformerGameMode_h_12_SPARSE_DATA
#define Platformer_Source_Platformer_PlatformerGameMode_h_12_RPC_WRAPPERS
#define Platformer_Source_Platformer_PlatformerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Platformer_Source_Platformer_PlatformerGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlatformerGameMode(); \
	friend struct Z_Construct_UClass_APlatformerGameMode_Statics; \
public: \
	DECLARE_CLASS(APlatformerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), PLATFORMER_API) \
	DECLARE_SERIALIZER(APlatformerGameMode)


#define Platformer_Source_Platformer_PlatformerGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPlatformerGameMode(); \
	friend struct Z_Construct_UClass_APlatformerGameMode_Statics; \
public: \
	DECLARE_CLASS(APlatformerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), PLATFORMER_API) \
	DECLARE_SERIALIZER(APlatformerGameMode)


#define Platformer_Source_Platformer_PlatformerGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PLATFORMER_API APlatformerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PLATFORMER_API, APlatformerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PLATFORMER_API APlatformerGameMode(APlatformerGameMode&&); \
	PLATFORMER_API APlatformerGameMode(const APlatformerGameMode&); \
public:


#define Platformer_Source_Platformer_PlatformerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PLATFORMER_API APlatformerGameMode(APlatformerGameMode&&); \
	PLATFORMER_API APlatformerGameMode(const APlatformerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PLATFORMER_API, APlatformerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlatformerGameMode)


#define Platformer_Source_Platformer_PlatformerGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Platformer_Source_Platformer_PlatformerGameMode_h_9_PROLOG
#define Platformer_Source_Platformer_PlatformerGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_SPARSE_DATA \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_RPC_WRAPPERS \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_INCLASS \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_PlatformerGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_SPARSE_DATA \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_PlatformerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class APlatformerGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_PlatformerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
