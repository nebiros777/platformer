// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLATFORMER_PlatformerCharacter_generated_h
#error "PlatformerCharacter.generated.h already included, missing '#pragma once' in PlatformerCharacter.h"
#endif
#define PLATFORMER_PlatformerCharacter_generated_h

#define Platformer_Source_Platformer_PlatformerCharacter_h_10_DELEGATE \
static inline void FOnCharDeath_DelegateWrapper(const FMulticastScriptDelegate& OnCharDeath) \
{ \
	OnCharDeath.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Platformer_Source_Platformer_PlatformerCharacter_h_9_DELEGATE \
struct _Script_Platformer_eventOnBonusScoresUpdate_Parms \
{ \
	int32 CurrentScores; \
}; \
static inline void FOnBonusScoresUpdate_DelegateWrapper(const FMulticastScriptDelegate& OnBonusScoresUpdate, int32 CurrentScores) \
{ \
	_Script_Platformer_eventOnBonusScoresUpdate_Parms Parms; \
	Parms.CurrentScores=CurrentScores; \
	OnBonusScoresUpdate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_SPARSE_DATA
#define Platformer_Source_Platformer_PlatformerCharacter_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMoveUp); \
	DECLARE_FUNCTION(execDeath); \
	DECLARE_FUNCTION(execSpawnImpulseFX); \
	DECLARE_FUNCTION(execFatting); \
	DECLARE_FUNCTION(execBeginPlayGenerateBonus); \
	DECLARE_FUNCTION(execIncreaseDifficulty); \
	DECLARE_FUNCTION(execGettingUpBonus); \
	DECLARE_FUNCTION(execAddBonusScoresForGettingUp);


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMoveUp); \
	DECLARE_FUNCTION(execDeath); \
	DECLARE_FUNCTION(execSpawnImpulseFX); \
	DECLARE_FUNCTION(execFatting); \
	DECLARE_FUNCTION(execBeginPlayGenerateBonus); \
	DECLARE_FUNCTION(execIncreaseDifficulty); \
	DECLARE_FUNCTION(execGettingUpBonus); \
	DECLARE_FUNCTION(execAddBonusScoresForGettingUp);


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlatformerCharacter(); \
	friend struct Z_Construct_UClass_APlatformerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlatformerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformerCharacter)


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlatformerCharacter(); \
	friend struct Z_Construct_UClass_APlatformerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlatformerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformerCharacter)


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlatformerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformerCharacter(APlatformerCharacter&&); \
	NO_API APlatformerCharacter(const APlatformerCharacter&); \
public:


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformerCharacter(APlatformerCharacter&&); \
	NO_API APlatformerCharacter(const APlatformerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlatformerCharacter)


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SideViewCameraComponent() { return STRUCT_OFFSET(APlatformerCharacter, SideViewCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(APlatformerCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__ImpulseArrow() { return STRUCT_OFFSET(APlatformerCharacter, ImpulseArrow); }


#define Platformer_Source_Platformer_PlatformerCharacter_h_12_PROLOG
#define Platformer_Source_Platformer_PlatformerCharacter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_SPARSE_DATA \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_RPC_WRAPPERS \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_INCLASS \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_PlatformerCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_SPARSE_DATA \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_PlatformerCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class APlatformerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_PlatformerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
