// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Platformer/PlatformerCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlatformerCharacter() {}
// Cross Module References
	PLATFORMER_API UFunction* Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Platformer();
	PLATFORMER_API UFunction* Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature();
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformerCharacter_NoRegister();
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformerCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Platformer, nullptr, "OnCharDeath__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics
	{
		struct _Script_Platformer_eventOnBonusScoresUpdate_Parms
		{
			int32 CurrentScores;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentScores;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::NewProp_CurrentScores = { "CurrentScores", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Platformer_eventOnBonusScoresUpdate_Parms, CurrentScores), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::NewProp_CurrentScores,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Platformer, nullptr, "OnBonusScoresUpdate__DelegateSignature", nullptr, nullptr, sizeof(_Script_Platformer_eventOnBonusScoresUpdate_Parms), Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execMoveUp)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MoveUp();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execDeath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Death();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execSpawnImpulseFX)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnImpulseFX();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execFatting)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Fatting();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execBeginPlayGenerateBonus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BeginPlayGenerateBonus();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execIncreaseDifficulty)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncreaseDifficulty();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execGettingUpBonus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GettingUpBonus();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlatformerCharacter::execAddBonusScoresForGettingUp)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddBonusScoresForGettingUp();
		P_NATIVE_END;
	}
	void APlatformerCharacter::StaticRegisterNativesAPlatformerCharacter()
	{
		UClass* Class = APlatformerCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddBonusScoresForGettingUp", &APlatformerCharacter::execAddBonusScoresForGettingUp },
			{ "BeginPlayGenerateBonus", &APlatformerCharacter::execBeginPlayGenerateBonus },
			{ "Death", &APlatformerCharacter::execDeath },
			{ "Fatting", &APlatformerCharacter::execFatting },
			{ "GettingUpBonus", &APlatformerCharacter::execGettingUpBonus },
			{ "IncreaseDifficulty", &APlatformerCharacter::execIncreaseDifficulty },
			{ "MoveUp", &APlatformerCharacter::execMoveUp },
			{ "SpawnImpulseFX", &APlatformerCharacter::execSpawnImpulseFX },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "AddBonusScoresForGettingUp", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "BeginPlayGenerateBonus", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlatformerCharacter_Death_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_Death_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_Death_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "Death", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_Death_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_Death_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_Death()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_Death_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlatformerCharacter_Fatting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_Fatting_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_Fatting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "Fatting", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_Fatting_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_Fatting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_Fatting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_Fatting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "GettingUpBonus", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "IncreaseDifficulty", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlatformerCharacter_MoveUp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_MoveUp_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//Move up\n" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
		{ "ToolTip", "Move up" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_MoveUp_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "MoveUp", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_MoveUp_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_MoveUp_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_MoveUp()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_MoveUp_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlatformerCharacter, nullptr, "SpawnImpulseFX", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APlatformerCharacter_NoRegister()
	{
		return APlatformerCharacter::StaticClass();
	}
	struct Z_Construct_UClass_APlatformerCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FattingIncreaseMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FattingIncreaseMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusAmmountOnBeginplay_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BonusAmmountOnBeginplay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DifficultyIncreaseMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DifficultyIncreaseMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusMinAddible_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BonusMinAddible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusMaxAddible_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BonusMaxAddible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusScores_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BonusScores;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImpulseFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImpulseFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JumpImpulse_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_JumpImpulse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCharDeath_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCharDeath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBonusScoresUpdate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBonusScoresUpdate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImpulseArrow_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImpulseArrow;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SideViewCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SideViewCameraComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlatformerCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_Platformer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APlatformerCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APlatformerCharacter_AddBonusScoresForGettingUp, "AddBonusScoresForGettingUp" }, // 2365930870
		{ &Z_Construct_UFunction_APlatformerCharacter_BeginPlayGenerateBonus, "BeginPlayGenerateBonus" }, // 3310210336
		{ &Z_Construct_UFunction_APlatformerCharacter_Death, "Death" }, // 2451516266
		{ &Z_Construct_UFunction_APlatformerCharacter_Fatting, "Fatting" }, // 2964503864
		{ &Z_Construct_UFunction_APlatformerCharacter_GettingUpBonus, "GettingUpBonus" }, // 1440486791
		{ &Z_Construct_UFunction_APlatformerCharacter_IncreaseDifficulty, "IncreaseDifficulty" }, // 3641429373
		{ &Z_Construct_UFunction_APlatformerCharacter_MoveUp, "MoveUp" }, // 2285270588
		{ &Z_Construct_UFunction_APlatformerCharacter_SpawnImpulseFX, "SpawnImpulseFX" }, // 1633976036
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PlatformerCharacter.h" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_FattingIncreaseMultiplier_MetaData[] = {
		{ "Category", "Bonus value" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_FattingIncreaseMultiplier = { "FattingIncreaseMultiplier", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, FattingIncreaseMultiplier), METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_FattingIncreaseMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_FattingIncreaseMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusAmmountOnBeginplay_MetaData[] = {
		{ "Category", "Bonus value" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusAmmountOnBeginplay = { "BonusAmmountOnBeginplay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, BonusAmmountOnBeginplay), METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusAmmountOnBeginplay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusAmmountOnBeginplay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_DifficultyIncreaseMultiplier_MetaData[] = {
		{ "Category", "Bonus value" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_DifficultyIncreaseMultiplier = { "DifficultyIncreaseMultiplier", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, DifficultyIncreaseMultiplier), METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_DifficultyIncreaseMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_DifficultyIncreaseMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMinAddible_MetaData[] = {
		{ "Category", "Bonus value" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMinAddible = { "BonusMinAddible", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, BonusMinAddible), METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMinAddible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMinAddible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMaxAddible_MetaData[] = {
		{ "Category", "Bonus value" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMaxAddible = { "BonusMaxAddible", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, BonusMaxAddible), METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMaxAddible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMaxAddible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusScores_MetaData[] = {
		{ "Category", "Bonus value" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusScores = { "BonusScores", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, BonusScores), METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusScores_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusScores_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseFX_MetaData[] = {
		{ "Category", "Impulse" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseFX = { "ImpulseFX", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, ImpulseFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseFX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_JumpImpulse_MetaData[] = {
		{ "Category", "Impulse" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_JumpImpulse = { "JumpImpulse", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, JumpImpulse), METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_JumpImpulse_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_JumpImpulse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnCharDeath_MetaData[] = {
		{ "Category", "Death" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnCharDeath = { "OnCharDeath", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, OnCharDeath), Z_Construct_UDelegateFunction_Platformer_OnCharDeath__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnCharDeath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnCharDeath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnBonusScoresUpdate_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnBonusScoresUpdate = { "OnBonusScoresUpdate", nullptr, (EPropertyFlags)0x0010000010080005, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, OnBonusScoresUpdate), Z_Construct_UDelegateFunction_Platformer_OnBonusScoresUpdate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnBonusScoresUpdate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnBonusScoresUpdate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseArrow_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseArrow = { "ImpulseArrow", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, ImpulseArrow), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseArrow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseArrow_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera beside the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera beside the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_SideViewCameraComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Side view camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlatformerCharacter.h" },
		{ "ToolTip", "Side view camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_SideViewCameraComponent = { "SideViewCameraComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformerCharacter, SideViewCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_SideViewCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_SideViewCameraComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlatformerCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_FattingIncreaseMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusAmmountOnBeginplay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_DifficultyIncreaseMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMinAddible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusMaxAddible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_BonusScores,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_JumpImpulse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnCharDeath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_OnBonusScoresUpdate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_ImpulseArrow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_CameraBoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformerCharacter_Statics::NewProp_SideViewCameraComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlatformerCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlatformerCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlatformerCharacter_Statics::ClassParams = {
		&APlatformerCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APlatformerCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlatformerCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformerCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlatformerCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlatformerCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlatformerCharacter, 3065733835);
	template<> PLATFORMER_API UClass* StaticClass<APlatformerCharacter>()
	{
		return APlatformerCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlatformerCharacter(Z_Construct_UClass_APlatformerCharacter, &APlatformerCharacter::StaticClass, TEXT("/Script/Platformer"), TEXT("APlatformerCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlatformerCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
