// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLATFORMER_Platformer_Enemy_AIController_generated_h
#error "Platformer_Enemy_AIController.generated.h already included, missing '#pragma once' in Platformer_Enemy_AIController.h"
#endif
#define PLATFORMER_Platformer_Enemy_AIController_generated_h

#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_SPARSE_DATA
#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMovingSomwhere);


#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMovingSomwhere);


#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlatformer_Enemy_AIController(); \
	friend struct Z_Construct_UClass_APlatformer_Enemy_AIController_Statics; \
public: \
	DECLARE_CLASS(APlatformer_Enemy_AIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_Enemy_AIController)


#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlatformer_Enemy_AIController(); \
	friend struct Z_Construct_UClass_APlatformer_Enemy_AIController_Statics; \
public: \
	DECLARE_CLASS(APlatformer_Enemy_AIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_Enemy_AIController)


#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlatformer_Enemy_AIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformer_Enemy_AIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_Enemy_AIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_Enemy_AIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_Enemy_AIController(APlatformer_Enemy_AIController&&); \
	NO_API APlatformer_Enemy_AIController(const APlatformer_Enemy_AIController&); \
public:


#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlatformer_Enemy_AIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_Enemy_AIController(APlatformer_Enemy_AIController&&); \
	NO_API APlatformer_Enemy_AIController(const APlatformer_Enemy_AIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_Enemy_AIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_Enemy_AIController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformer_Enemy_AIController)


#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_PRIVATE_PROPERTY_OFFSET
#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_12_PROLOG
#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_SPARSE_DATA \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_RPC_WRAPPERS \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_INCLASS \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_SPARSE_DATA \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Platformer_Enemy_AIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class APlatformer_Enemy_AIController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_Platformer_Enemy_AIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
