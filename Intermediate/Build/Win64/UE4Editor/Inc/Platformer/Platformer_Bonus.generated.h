// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PLATFORMER_Platformer_Bonus_generated_h
#error "Platformer_Bonus.generated.h already included, missing '#pragma once' in Platformer_Bonus.h"
#endif
#define PLATFORMER_Platformer_Bonus_generated_h

#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_SPARSE_DATA
#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnGettingFX); \
	DECLARE_FUNCTION(execBulletCollisionSphereBeginOverlap);


#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnGettingFX); \
	DECLARE_FUNCTION(execBulletCollisionSphereBeginOverlap);


#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlatformer_Bonus(); \
	friend struct Z_Construct_UClass_APlatformer_Bonus_Statics; \
public: \
	DECLARE_CLASS(APlatformer_Bonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_Bonus)


#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlatformer_Bonus(); \
	friend struct Z_Construct_UClass_APlatformer_Bonus_Statics; \
public: \
	DECLARE_CLASS(APlatformer_Bonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Platformer"), NO_API) \
	DECLARE_SERIALIZER(APlatformer_Bonus)


#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlatformer_Bonus(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlatformer_Bonus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_Bonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_Bonus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_Bonus(APlatformer_Bonus&&); \
	NO_API APlatformer_Bonus(const APlatformer_Bonus&); \
public:


#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlatformer_Bonus(APlatformer_Bonus&&); \
	NO_API APlatformer_Bonus(const APlatformer_Bonus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlatformer_Bonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlatformer_Bonus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlatformer_Bonus)


#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_PRIVATE_PROPERTY_OFFSET
#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_12_PROLOG
#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_SPARSE_DATA \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_RPC_WRAPPERS \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_INCLASS \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_PRIVATE_PROPERTY_OFFSET \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_SPARSE_DATA \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_INCLASS_NO_PURE_DECLS \
	Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLATFORMER_API UClass* StaticClass<class APlatformer_Bonus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Platformer_Source_Platformer_Enviroment_Platformer_Bonus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
