// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Platformer/Enviroment/Platformer_AreaToSpawnBonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlatformer_AreaToSpawnBonus() {}
// Cross Module References
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformer_AreaToSpawnBonus_NoRegister();
	PLATFORMER_API UClass* Z_Construct_UClass_APlatformer_AreaToSpawnBonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Platformer();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void APlatformer_AreaToSpawnBonus::StaticRegisterNativesAPlatformer_AreaToSpawnBonus()
	{
	}
	UClass* Z_Construct_UClass_APlatformer_AreaToSpawnBonus_NoRegister()
	{
		return APlatformer_AreaToSpawnBonus::StaticClass();
	}
	struct Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Platformer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Enviroment/Platformer_AreaToSpawnBonus.h" },
		{ "ModuleRelativePath", "Enviroment/Platformer_AreaToSpawnBonus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Enviroment/Platformer_AreaToSpawnBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlatformer_AreaToSpawnBonus, StaticMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::NewProp_StaticMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::NewProp_StaticMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlatformer_AreaToSpawnBonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::ClassParams = {
		&APlatformer_AreaToSpawnBonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlatformer_AreaToSpawnBonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlatformer_AreaToSpawnBonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlatformer_AreaToSpawnBonus, 1975373488);
	template<> PLATFORMER_API UClass* StaticClass<APlatformer_AreaToSpawnBonus>()
	{
		return APlatformer_AreaToSpawnBonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlatformer_AreaToSpawnBonus(Z_Construct_UClass_APlatformer_AreaToSpawnBonus, &APlatformer_AreaToSpawnBonus::StaticClass, TEXT("/Script/Platformer"), TEXT("APlatformer_AreaToSpawnBonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlatformer_AreaToSpawnBonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
