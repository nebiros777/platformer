// Fill out your copyright notice in the Description page of Project Settings.


#include "Platformer_EnemyCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include <Platformer\PlatformerCharacter.h>
#include "Enviroment/Platformer_Bonus.h"
#include "Platformer_GameInstance.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
APlatformer_EnemyCharacter::APlatformer_EnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Impulse = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Impulse"));
	Impulse->SetupAttachment(GetMesh());

	GetCharacterMovement()->GravityScale=0.0f;

	GetMesh()->SetCollisionProfileName(TEXT("NoCollision"));

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &APlatformer_EnemyCharacter::BeginOverlap);
}

// Called when the game starts or when spawned
void APlatformer_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlatformer_EnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlatformer_EnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlatformer_EnemyCharacter::BeginOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlatformerCharacter* myChar = Cast<APlatformerCharacter>(OtherActor);
	if (myChar)
	{
		myChar->Death();
	}

	APlatformer_Bonus* myBonus = Cast<APlatformer_Bonus>(OtherActor);
	if (myBonus)
	{
		UPlatformer_GameInstance* myGI = Cast<UPlatformer_GameInstance>(GetWorld()->GetGameInstance());

		if (myGI)
		{
			int32 FoundIndex;
			myGI->SpawnedBonuses.Find(myBonus, FoundIndex);
			if (FoundIndex >= 0)
			{
				myGI->SpawnedBonuses.Remove(myBonus);
				
				
			}
			if (GettingSound)
			{
				UGameplayStatics::PlaySound2D(GetWorld(), GettingSound);
			}
			myBonus->Destroy();
			myGI->GenerateBonus();
		}
		
	}


}

