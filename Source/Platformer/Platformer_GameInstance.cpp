// Fill out your copyright notice in the Description page of Project Settings.


#include "Platformer_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/BoxSphereBounds.h"





void UPlatformer_GameInstance::GenerateBonus()
{
	
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(GetRandomPointInArea());
	SpawnTransform.SetRotation(UKismetMathLibrary::Quat_MakeFromEuler(FVector(0, 0, 0)));
	SpawnTransform.SetScale3D(FVector(1.f, 1.f, 1.f));

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	
	APlatformer_Bonus* myBonus = Cast<APlatformer_Bonus>(GetWorld()->SpawnActor(SpawnBonus, &SpawnTransform, SpawnParams));

	SpawnedBonuses.Add(myBonus);
	OnBonusUpdate.Broadcast();
}

void UPlatformer_GameInstance::SpawnEnemy()
{
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(GetRandomPointInArea());
	SpawnTransform.SetRotation(UKismetMathLibrary::Quat_MakeFromEuler(FVector(0, 0, 0)));
	SpawnTransform.SetScale3D(FVector(1.f, 1.f, 1.f));

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	APlatformer_EnemyCharacter* myEnemy = Cast<APlatformer_EnemyCharacter>(GetWorld()->SpawnActor(SpawnedEnemy, &SpawnTransform, SpawnParams));

}

FVector UPlatformer_GameInstance::GetRandomPointInArea()
{
	TArray <AActor*> SpawnBoxes;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), SpawnAreaBP, SpawnBoxes);
	if (SpawnBoxes.Num()>0)
	{
	APlatformer_AreaToSpawnBonus* myArea = Cast<APlatformer_AreaToSpawnBonus>(SpawnBoxes[UKismetMathLibrary::RandomIntegerInRange(0, SpawnBoxes.Num() - 1)]);
	if (myArea)
	{
		FBoxSphereBounds ComponentBounds = myArea->StaticMesh->Bounds;
		FVector NewBoxExtent = FVector(ComponentBounds.BoxExtent.X, ComponentBounds.BoxExtent.Y, ComponentBounds.BoxExtent.Z);
		FVector RandomPoint = UKismetMathLibrary::RandomPointInBoundingBox(ComponentBounds.Origin, NewBoxExtent);


		return FVector(1200.0f, RandomPoint.Y, RandomPoint.Z);
	}
	}

	return FVector(0);

}
