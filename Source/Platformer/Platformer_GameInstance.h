// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Enviroment/Platformer_AreaToSpawnBonus.h"
#include "Platformer_EnemyCharacter.h"
#include "Enviroment/Platformer_Bonus.h"
#include "Platformer_GameInstance.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBonusUpdate);

UCLASS()
class PLATFORMER_API UPlatformer_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	

public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Bonus")
	FOnBonusUpdate OnBonusUpdate;

	UFUNCTION(BlueprintCallable)
		void GenerateBonus();

	UFUNCTION(BlueprintCallable)
		void SpawnEnemy();

	UFUNCTION(BlueprintCallable)
		FVector GetRandomPointInArea();

 	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "SpawnSetting")
 	TSubclassOf<APlatformer_AreaToSpawnBonus> SpawnAreaBP;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "SpawnSetting")
		TSubclassOf<APlatformer_Bonus> SpawnBonus;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "SpawnSetting")
		TArray<APlatformer_Bonus*> SpawnedBonuses;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "SpawnSetting")
		TSubclassOf<APlatformer_EnemyCharacter> SpawnedEnemy;
};
