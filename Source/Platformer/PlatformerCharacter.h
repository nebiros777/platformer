// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlatformerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBonusScoresUpdate, int32, CurrentScores);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharDeath);

UCLASS(config=Game)
class APlatformerCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class UArrowComponent* ImpulseArrow = nullptr;

protected:
	

	/** Called for side to side input */
	void MoveRight();
	void MoveLeft();
	void Lower();

	
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	virtual void BeginPlay() override;

public:
	APlatformerCharacter();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		FOnBonusScoresUpdate OnBonusScoresUpdate;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Death")
		FOnCharDeath OnCharDeath;

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Impulse")
		float JumpImpulse = 400.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Impulse")
		UParticleSystem* ImpulseFX = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus value")
		int32 BonusScores = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus value")
		int32 BonusMaxAddible = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus value")
		int32 BonusMinAddible = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus value")
	float DifficultyIncreaseMultiplier = 1.01f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus value")
		int32  BonusAmmountOnBeginplay = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus value")
		float FattingIncreaseMultiplier = 1.05f;

	
	
	UFUNCTION()
		void AddBonusScoresForGettingUp();

	UFUNCTION()
		void GettingUpBonus();

	UFUNCTION()
		void IncreaseDifficulty();

	UFUNCTION(BlueprintCallable)
		void BeginPlayGenerateBonus();

	UFUNCTION()
		void Fatting();

	UFUNCTION()
		void SpawnImpulseFX();

	UFUNCTION()
		void Death();
	
	UFUNCTION()
		//Move up
		void MoveUp();
};
