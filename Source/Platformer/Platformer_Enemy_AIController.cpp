// Fill out your copyright notice in the Description page of Project Settings.


#include "Platformer_Enemy_AIController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/BoxSphereBounds.h"
#include "Platformer_GameInstance.h"


void APlatformer_Enemy_AIController::MovingSomwhere()
{
	UPlatformer_GameInstance* myGI = Cast<UPlatformer_GameInstance>(GetGameInstance());
	if (myGI)
	{
	MoveToLocation(myGI->GetRandomPointInArea());
	}
}

