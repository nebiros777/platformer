// Fill out your copyright notice in the Description page of Project Settings.


#include "Platformer_AreaToSpawnBonus.h"


// Sets default values
APlatformer_AreaToSpawnBonus::APlatformer_AreaToSpawnBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnAreaMesh"));
	StaticMesh->SetCollisionProfileName(TEXT("Custom"));
	StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	StaticMesh->SetEnableGravity(false);
	StaticMesh->SetSimulatePhysics(false);
	StaticMesh->SetVisibility(true);
	StaticMesh->SetHiddenInGame(true);
	RootComponent = StaticMesh;

}

// Called when the game starts or when spawned
void APlatformer_AreaToSpawnBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlatformer_AreaToSpawnBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

