// Fill out your copyright notice in the Description page of Project Settings.


#include "Platformer_Bonus.h"

#include "Kismet/KismetMathLibrary.h"
#include <Platformer\PlatformerCharacter.h>
#include "../Platformer_GameInstance.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
APlatformer_Bonus::APlatformer_Bonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BonusCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision sphere"));
	BonusCollisionSphere->SetSphereRadius(60.0f);
	BonusCollisionSphere->bReturnMaterialOnMove = true;
	BonusCollisionSphere->SetCanEverAffectNavigation(false);
	
	RootComponent = BonusCollisionSphere;

	BonusStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BonusStaticMesh->SetupAttachment(RootComponent);
	BonusStaticMesh->SetCanEverAffectNavigation(false);
	BonusStaticMesh->SetRelativeScale3D(FVector(10.f));
	BonusStaticMesh->SetCollisionProfileName(TEXT("Custom"));
	BonusStaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	BonusStaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
}

// Called when the game starts or when spawned
void APlatformer_Bonus::BeginPlay()
{
	Super::BeginPlay();
	BonusCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &APlatformer_Bonus::BulletCollisionSphereBeginOverlap);
}

// Called every frame
void APlatformer_Bonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlatformer_Bonus::BulletCollisionSphereBeginOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
int32 FoundIndex = -1;
	if (OtherActor)
	{
		APlatformerCharacter* myCharacter = Cast<APlatformerCharacter>(OtherActor);

		if (myCharacter)
		{
			myCharacter->GettingUpBonus();
			//GEngine->AddOnScreenDebugMessage(0, 1, FColor::Green, FString::SanitizeFloat(UKismetMathLibrary::FTrunc(myCharacter->BonusScores)));
			
			UPlatformer_GameInstance* myGI = Cast<UPlatformer_GameInstance>(GetWorld()->GetGameInstance());

			if (myGI)
			{
				myGI->SpawnedBonuses.Find(this, FoundIndex);
				if (FoundIndex>=0)
				{
					myGI->SpawnedBonuses.Remove(this);
					
				}
				
				myGI->GenerateBonus();
				
			}

			SpawnGettingFX();
			this->Destroy();
			
		}



	}
}

void APlatformer_Bonus::SpawnGettingFX()
{
	if (GettingFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GettingFX, BonusStaticMesh->GetComponentTransform());
	}
	if (GettingSound)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), GettingSound);
	}

}

