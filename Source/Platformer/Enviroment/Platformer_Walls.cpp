// Fill out your copyright notice in the Description page of Project Settings.


#include "Platformer_Walls.h"
#include "../PlatformerCharacter.h"

// Sets default values
APlatformer_Walls::APlatformer_Walls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallMesh"));
	StaticMesh->SetCollisionProfileName(TEXT("Custom"));
	StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	StaticMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	StaticMesh->SetEnableGravity(false);
	StaticMesh->SetSimulatePhysics(false);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &APlatformer_Walls::BulletCollisionSphereBeginOverlap);
	RootComponent = StaticMesh;
}

// Called when the game starts or when spawned
void APlatformer_Walls::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlatformer_Walls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlatformer_Walls::BulletCollisionSphereBeginOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlatformerCharacter* myChar = Cast<APlatformerCharacter>(OtherActor);
	if (myChar)
	{
		myChar->Death();
	}

}

