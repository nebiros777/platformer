// Fill out your copyright notice in the Description page of Project Settings.


#include "Platformer_Teleport.h"

#include "../PlatformerCharacter.h"

// Sets default values
APlatformer_Teleport::APlatformer_Teleport()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;
	
	TopStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Top Static Mesh"));
	TopStaticMesh->SetupAttachment(RootComponent);
	TopStaticMesh->SetCanEverAffectNavigation(false);

 	TopDownArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Top Down Arrow"));
 	TopDownArrow->SetupAttachment(TopStaticMesh);

	TopTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Top Trigger"));
	TopTrigger->SetupAttachment(TopStaticMesh);
	
	BottomStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bottom Static Mesh"));
	BottomStaticMesh->SetupAttachment(RootComponent);
	BottomStaticMesh->SetCanEverAffectNavigation(false);

 	BottomUpArrow  = CreateDefaultSubobject<UArrowComponent>(TEXT("Bottom Up Arrow"));
 	BottomUpArrow->SetupAttachment(BottomStaticMesh);

	BottomTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Bottom Trigger"));
	BottomTrigger->SetupAttachment(BottomStaticMesh);
	
}

// Called when the game starts or when spawned
void APlatformer_Teleport::BeginPlay()
{
	Super::BeginPlay();
	TopTrigger->OnComponentBeginOverlap.AddDynamic(this, &APlatformer_Teleport::TopTriggerBeginOverlap);
	BottomTrigger->OnComponentBeginOverlap.AddDynamic(this, &APlatformer_Teleport::BottomTriggerBeginOverlap);

}

// Called every frame
void APlatformer_Teleport::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlatformer_Teleport::TopTriggerBeginOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		APlatformerCharacter* myChar = Cast<APlatformerCharacter>(OtherActor);
		if (myChar)
		{
			
			myChar->SetActorLocation(BottomUpArrow->GetComponentLocation());
			myChar->MoveUp();
		}
		
	}
}

void APlatformer_Teleport::BottomTriggerBeginOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		APlatformerCharacter* myChar = Cast<APlatformerCharacter>(OtherActor);
		if (myChar)
		{
			
			myChar->SetActorLocation(TopDownArrow->GetComponentLocation());
			
		}

	}
}

