// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlatformerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Platformer_GameInstance.h"
#include "Kismet/GameplayStatics.h"

APlatformerCharacter::APlatformerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 2500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f,180.f,0.f));

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 0.4f;
	GetCharacterMovement()->AirControl = 0.1f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 10.f;
	GetCharacterMovement()->MaxFlySpeed = 10.f;

	ImpulseArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ImpulseArrow"));
	ImpulseArrow->SetupAttachment(GetMesh());

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void APlatformerCharacter::AddBonusScoresForGettingUp()
{
	BonusScores += UKismetMathLibrary::RandomIntegerInRange(BonusMinAddible, BonusMaxAddible);
	OnBonusScoresUpdate.Broadcast(BonusScores);
}

void APlatformerCharacter::GettingUpBonus()
{
	AddBonusScoresForGettingUp();
	IncreaseDifficulty();
}

void APlatformerCharacter::IncreaseDifficulty()
{
	GetCharacterMovement()->GravityScale *= DifficultyIncreaseMultiplier;
	GetCharacterMovement()->AirControl *= DifficultyIncreaseMultiplier;
	JumpImpulse /= DifficultyIncreaseMultiplier;
	Fatting();

}

void APlatformerCharacter::BeginPlayGenerateBonus()
{
	UPlatformer_GameInstance* myGI = Cast<UPlatformer_GameInstance>(GetWorld()->GetGameInstance());

	if (myGI)
	{
		myGI->SpawnEnemy();
// 		if (myGI->SpawnedBonuses.Num() > 0)
// 		{
// 			for (int32 i = myGI->SpawnedBonuses.Num()-1; i >-1 ;i--)
// 			{
// 				myGI->SpawnedBonuses.Remove(myGI->SpawnedBonuses[i]);
// 			}
// 		}
		int32 i = 0;
		while (i<BonusAmmountOnBeginplay)
		{		
			myGI->GenerateBonus();
			i++;
		}
	}
}

void APlatformerCharacter::Fatting()
{
	FVector CurrentScale = GetMesh() ->GetRelativeScale3D();
	GetMesh()->SetRelativeScale3D(FVector(CurrentScale.X* FattingIncreaseMultiplier,
										CurrentScale.Y* FattingIncreaseMultiplier,
										CurrentScale.Z));
}

void APlatformerCharacter::SpawnImpulseFX()
{
	if (ImpulseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpulseFX, ImpulseArrow->GetComponentTransform());
	}
	
}

void APlatformerCharacter::Death()
{
	OnCharDeath.Broadcast();
}

//////////////////////////////////////////////////////////////////////////
// Input

void APlatformerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlatformerCharacter::MoveUp);
	PlayerInputComponent->BindAction("MoveRight", IE_Pressed, this, &APlatformerCharacter::MoveRight);
	PlayerInputComponent->BindAction("MoveLeft", IE_Pressed, this, &APlatformerCharacter::MoveLeft);
	PlayerInputComponent->BindAction("Lower", IE_Pressed, this, &APlatformerCharacter::Lower);
	
}

void APlatformerCharacter::BeginPlay()
{
	Super::BeginPlay();
	//BeginPlayGenerateBonus();
}

void APlatformerCharacter::MoveUp()
{	
	Cast<UCharacterMovementComponent>(this->GetMovementComponent())->AddImpulse(FVector(0.0f, 0.0f, JumpImpulse), true);
	SpawnImpulseFX();
}


void APlatformerCharacter::MoveRight()
{
	
	Cast<UCharacterMovementComponent>(this->GetMovementComponent())->AddImpulse(FVector(0.0f, -JumpImpulse,  JumpImpulse), true);
	FRotator CurrentRotation = GetMesh()->GetRelativeRotation();
	GetMesh()->SetRelativeRotation(FRotator(CurrentRotation.Pitch, 270.0f,CurrentRotation.Roll));
	SpawnImpulseFX();
}

void APlatformerCharacter::MoveLeft()
{
	Cast<UCharacterMovementComponent>(this->GetMovementComponent())->AddImpulse(FVector(0.0f, JumpImpulse, JumpImpulse), true);
	FRotator CurrentRotation = GetMesh()->GetRelativeRotation();
	GetMesh()->SetRelativeRotation(FRotator(CurrentRotation.Pitch, 90.0f, CurrentRotation.Roll));
	SpawnImpulseFX();
}

void APlatformerCharacter::Lower()
{
	Cast<UCharacterMovementComponent>(this->GetMovementComponent())->AddImpulse(FVector(0.0f, 0.0f, -JumpImpulse), true);

}

