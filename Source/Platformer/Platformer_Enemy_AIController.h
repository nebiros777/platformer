// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Platformer_Enemy_AIController.generated.h"

/**
 * 
 */
UCLASS()
class PLATFORMER_API APlatformer_Enemy_AIController : public AAIController
{
	GENERATED_BODY()
	
public:
	

	UFUNCTION(BlueprintCallable)
		void MovingSomwhere();

	
};
